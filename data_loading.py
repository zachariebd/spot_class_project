import logging
from os import listdir
from os.path import splitext
from pathlib import Path
import rasterio as rio
import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset
import torchvision.transforms as transforms

class BasicDataset(Dataset):
    def __init__(self, images_dir: str, masks_dir: str, scale: float = 1.0, mask_suffix: str = ''):
        self.images_dir = Path(images_dir)
        self.masks_dir = Path(masks_dir)
        assert 0 < scale <= 1, 'Scale must be between 0 and 1'
        self.scale = scale
        self.mask_suffix = mask_suffix

        self.ids = [splitext(file)[0] for file in listdir(images_dir) if not file.startswith('.')]
        if not self.ids:
            raise RuntimeError(f'No input file found in {images_dir}, make sure you put your images there')
        logging.info(f'Creating dataset with {len(self.ids)} examples')

    def __len__(self):
        return len(self.ids)





    def __getitem__(self, idx):
        name = self.ids[idx]
        #print('name',name)
        mask_file = list(self.masks_dir.glob(name  + '.*'))
        img_file = list(self.images_dir.glob(name + '.*'))
        #print('masks',mask_file)
        #print('img',img_file)

        mask = rio.open(mask_file[0])
        img = rio.open(img_file[0])
        
        
        
        
        img_array = img.read()
        mask_array = mask.read()
        
        #img_array *= 1.0/img_array.max() #img_array  to max 1.0 value 


        #trans = transforms.Compose([transforms.ToTensor()])
        #tensor_img = trans(img_array).permute(1,2,0)
        tensor_img = torch.as_tensor(img_array).float()
        tensor_mask = torch.squeeze(torch.as_tensor(mask_array)).float()
        #print("tensor img",tensor_img.size(), tensor_img.type())
        #print(tensor_img)
        #print("tensor mask",tensor_mask.size(),tensor_mask.type())
        #print(tensor_mask)

        

        return {
            'image': tensor_img,
            'mask': tensor_mask
        }

